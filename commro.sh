#!/bin/bash
# Commit Roulette
# ~~ Holden Lewis ~~

MESSAGE="$(curl whatthecommit.com 2>/dev/null | grep ^\<p\> | sed -e 's/^<p>//')"

if [[ "$1" == "commit" ]]; then
    git commit -m "$MESSAGE"
else
    echo "$MESSAGE"
fi

